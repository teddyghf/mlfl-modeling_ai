# MLFL modeling_AI

This project contains core codes and the link to access the dataset for modeling the mode-locked fiber laser using AI. The codes have been written using Pytorch.


Codes
--------------

``LSTM_model_Batch_v4.py``
The main script is used for creating, training and testing the model. Training can be started by setting `train_sign=1`. Testing can be started by setting `train_sign=0` and `loss_plot=0`. 

``MyDataset_LSTM_Batch_v2.py``
The script defines the dataset class and the model class.


Dataset
--------------
[The corresponding dataset](https://sandbox.zenodo.org/record/1112053#.Y0Er9bZBxD8) can be downloaded used for academic purposes only.