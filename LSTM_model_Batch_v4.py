import torch
import torch.nn as nn
from MyDataset_LSTM_Batch_v2 import MyDataset_LSTM_Batch_v2
from MyDataset_LSTM_Batch_v2 import LSTM
from torch.utils import data
import numpy as np
import matplotlib.pyplot as plt
import time
import os
import scipy.io as sio
import seaborn as sns
import mat73

# Hyper parameters
test_plot = 0
train_sign = 0
val_sign = 1
loss_plot = 0
steps_print = 20
step_test_plot = 1
num_pts = 256
data_dim = num_pts*2  # size w/o par
n_para = 2
n_cell = 1024  # hidden_dim
n_output = data_dim
lstm_layer = 2
dense_layer = 1
lstm_bidirectional = False
shuffle = True
window = 14
BATCH_SIZE = 256
EPOCH = 500
LR = 0.001
WEIGHT_DECAY = 0
LR_DECAY = 1  # 1: no learning rate decay
LR_DECAY_STEP_SIZE = 40
BATCH_NORM = 'On'
dropout_lstm = 0.2
num_workers = 4
# Simulation
root = "Batches_time_real_imag_interleaved_G0 2-4.5_100-200MHz_5ps 1pJ_seed 1/"
train_path = root + "train/"
val_path = root + "val/"
test_path = root + "test/"
save_path = "results/"      # the path to keep train and test results
Hyper_list = []
Hyper_list.append("DATASET: " + root)
Hyper_list.append("BATCH_SIZE: " + str(BATCH_SIZE))
Hyper_list.append("EPOCH: " + str(EPOCH))
Hyper_list.append("Learning rate: " + str(LR))
Hyper_list.append("WEIGHT_DECAY: " + str(WEIGHT_DECAY))
Hyper_list.append("LR_DECAY: " + str(LR_DECAY))
Hyper_list.append("dropout_lstm: " + str(dropout_lstm))
Hyper_list.append("BATCH_NORM: " + BATCH_NORM)
Hyper_list.append("data_dim: " + str(data_dim))
Hyper_list.append("n_cell: " + str(n_cell))
Hyper_list.append("n_output: " + str(n_output))
Hyper_list.append("window: " + str(window))
Hyper_list.append("lstm_layer: " + str(lstm_layer))
Hyper_list.append("lstm_bidirectional: " + str(lstm_bidirectional))
Hyper_list.append("dense_layer: " + str(dense_layer))
Hyper_list.append("shuffle: " + str(shuffle))


# Fix the seed
def setup_seed(seed):
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    torch.backends.cudnn.deterministic = True


def train():
    # model
    LSTM_Net = LSTM(in_dim=data_dim,
                    hidden_dim=n_cell,
                    n_class=n_output,
                    bidirectional=lstm_bidirectional,
                    n_layer=lstm_layer,
                    n_dense=dense_layer,
                    n_para=n_para,
                    window=window,
                    dropout=dropout_lstm)

    # optimizer and loss
    optimizer = torch.optim.Adam(LSTM_Net.parameters(), lr=LR, weight_decay=WEIGHT_DECAY)
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=LR_DECAY_STEP_SIZE, gamma=LR_DECAY)
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, EPOCH, eta_min=0, last_epoch=-1, verbose=True)
    loss_func = nn.MSELoss()
    loss_records = []
    train_mean_loss = []
    val_mean_loss = []
    lr_hist = []

    LSTM_Net.to(device)
    for epoch in range(EPOCH):
        print('\n=================EPOCH %d===================' % epoch)
        # print('Current learning rate: ', scheduler.get_lr()[0])
        scheduler.step()
        lr_hist.append(scheduler.get_lr()[0])
        for train_batch_idx in range(n_train_batches):
            # train dataset process
            train_data = MyDataset_LSTM_Batch_v2(root, train_path, train_batch_idx, window, data_dim=data_dim, train=True)
            train_loader = data.DataLoader(dataset=train_data,
                                           batch_size=BATCH_SIZE,
                                           shuffle=shuffle,
                                           num_workers=num_workers,
                                           pin_memory=True)
            # training process
            loss_records.append([])
            for step, (input, label, para) in enumerate(train_loader):
                b_x = input.view(-1, window, data_dim).to(device)
                b_y = label.view(-1, n_output).to(device)
                if n_para:
                    para = para.view(-1, n_para).to(device)
                    output = LSTM_Net(b_x, para)
                else:
                    output = LSTM_Net(b_x)
                loss = loss_func(output, b_y)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                loss_records[epoch].append(loss.cpu().data.numpy())
                if step % steps_print == 0:
                    print('Epoch: ', epoch, ' | Step: ', step, '| train loss: %.4e' % loss.cpu().data.numpy())
        print('Epoch: ', epoch, '| Mean train loss: %.4e' % np.mean(loss_records[epoch]))
        train_mean_loss.append(np.mean(loss_records[epoch]))

        if val_sign:
            # validation dataset loading
            val_loss_records = []
            for val_batch_idx in range(n_val_batches):
                val_data = MyDataset_LSTM_Batch_v2(root, val_path, val_batch_idx, window, data_dim=data_dim, train=False,
                                                test=False)
                val_loader = data.DataLoader(dataset=val_data,
                                             batch_size=BATCH_SIZE,
                                             shuffle=shuffle,
                                             num_workers=num_workers,
                                             pin_memory=True)
                val_loss = val(LSTM_Net, val_loader, epoch)
                val_loss_records.append(val_loss)
            val_mean_loss.append(np.mean(val_loss_records))
    torch.save(LSTM_Net.state_dict(), save_path + 'latest.pkl')  # only save the parameters
    np.savetxt(save_path + "Training_Mean_Loss.txt", train_mean_loss)
    if val_sign:
        np.savetxt(save_path + "Val_Mean_Loss.txt", val_mean_loss)
    np.savetxt(save_path + "LR_hist.txt", lr_hist)


def val(model, val_loader, epoch):
    # set the model to validation mode
    model.eval()
    loss_func = nn.MSELoss()
    loss_records = []
    for step, (input, label, para) in enumerate(val_loader):
        b_x = input.view(-1, window, data_dim).to(device)
        b_y = label.view(-1, n_output).to(device)
        if n_para:
            para = para.view(-1, n_para).to(device)
            prediction = model(b_x, para)
        else:
            prediction = model(b_x)
        loss = loss_func(prediction, b_y)
        loss_records.append(loss.cpu().data.numpy())
    # np.savetxt(save_path + "Validation_Loss_Epoch_" + str(epoch) + ".txt", loss_records)
    # set the model back to train mode
    model.train()
    print('Epoch: ', epoch, '| Mean val loss: %.4e' % np.mean(loss_records))
    return np.mean(loss_records)


def test():
    model = restore_params()
    model.eval()
    loss_func = nn.MSELoss()
    loss_records = []
    # moving to device
    model.to(device)
    plot_cnt = 0

    print('\n=================Test===================')
    for test_batch_idx in range(n_test_batches):
        test_data = MyDataset_LSTM_Batch_v2(root, test_path, test_batch_idx, window, data_dim=data_dim, train=False, test=True)
        test_loader = data.DataLoader(dataset=test_data,
                                      batch_size=BATCH_SIZE,
                                      shuffle=False,
                                      num_workers=num_workers)
        for step, (input, label, para) in enumerate(test_loader):
            b_x = input.view(-1, window, data_dim).to(device)
            b_y = label.view(-1, n_output).to(device)
            if n_para:
                para = para.view(-1, n_para).to(device)
                prediction = model(b_x, para)
            else:
                prediction = model(b_x)
            loss = loss_func(prediction, b_y)
            loss_cpu = loss.cpu()
            loss_records.append(loss_cpu.data.numpy())
            # block mode: for saving
            for k in range(len(b_y)):
                # save_predictions("predictions", prediction[k].cpu().data.numpy())
                plot_cnt = plot_cnt + 1
                if test_plot and plot_cnt % step_test_plot == 0:
                    plt.clf()
                    plt.plot(b_y[k].cpu().numpy(), 'r-', lw=1, label='GT')
                    plt.plot(prediction[k].cpu().data.numpy(), 'b-', lw=1, label='Predicted')
                    plt.xlabel('Sampling points')
                    plt.ylabel('Intensity (a.u.)')
                    plt.legend(loc='upper right')
                    plt.title('Fitting result during test')
                    plt.savefig(save_path + 'test-' + str(plot_cnt) + '.png')
    np.savetxt(save_path + "Test_Loss.txt", loss_records)
    print('Final mean test loss: %.4e' % np.mean(loss_records))
    return np.mean(loss_records)


def test_v2():
    model = restore_params()
    model.eval()
    # moving to device
    model.to(device)
    # torch.backends.cudnn.enabled = False

    print('\n=================Test-v2===================')
    for test_batch_idx in range(n_test_batches):
        test_data = MyDataset_LSTM_Batch_v2(root, test_path, test_batch_idx, window, data_dim=data_dim, train=False, test=True)
        # totol_frames = test_data.n_steps
        totol_frames = 500
        pred = np.zeros((test_data.n_samples, totol_frames, data_dim))
        # get initial frame
        input, label, para = test_data.getStartFrames()
        # input dim: n_samples x window x data_dim
        pred[:, 0, :] = input[:, -1, :]  # save the initial to the predication
        total_time = 1
        start = time.time()
        for kk in range(total_time):
            for step in range(totol_frames-1):
                b_x = input.view(-1, window, data_dim).to(device)
                if n_para:
                    para = para.view(-1, n_para).to(device)
                    prediction = model(b_x, para)
                else:
                    prediction = model(b_x)
                pred[:, step + 1, :] = prediction.cpu().data.numpy()
                # 1 step leftshift for the input
                input[:, :-1, :] = input[:, 1:, :]
                input[:, -1, :] = prediction
            pred = test_data.anti_normalization(pred)
            # interleaved
            field = pred[:, :, ::2] + 1j*pred[:, :, 1::2]
            # concatenated
            # field = pred[:, :, :num_pts] + 1j*pred[:, :, num_pts:]
            amp = np.abs(field)
            phase = np.unwrap(np.angle(field), axis=2)
        end = time.time()
        print(str(total_time), '-time Inference time: ', end - start, ' sec')
        HyperFile = open(save_path + "Hyper_para.txt", "a")
        HyperFile.write(str(total_time) + '-time Inference time: ' + str(end - start) + ' sec' + "\n")
        HyperFile.close()
        sio.savemat(save_path + "Field_" + str(test_batch_idx + 1) + ".mat", {'field': field})
        sio.savemat(save_path + "Amp_" + str(test_batch_idx + 1) + ".mat", {'amp': amp})
        sio.savemat(save_path + "Phase_" + str(test_batch_idx + 1) + ".mat", {'phase': phase})


def HeatPlot_Amp(test_batch_idx, idx):
    sns.set_theme()
    # sns.set(style='whitegrid', color_codes=True)
    # label plot
    plt.clf()
    plt.subplot(311)
    path_dir = os.listdir(test_path)
    mat_contents = mat73.loadmat(test_path + path_dir[0])
    raw_data = mat_contents['data']
    dimension = raw_data.shape
    field_idx = raw_data[idx, :, :data_dim:2] + 1j*raw_data[idx, :, 1:data_dim:2]
    # field_idx = raw_data[idx, :, :num_pts] + 1j*raw_data[idx, :, num_pts:data_dim]
    field_idx = np.squeeze(field_idx).transpose()
    label_idx = np.abs(field_idx)
    fz = 14
    ax = sns.heatmap(label_idx, xticklabels=False)
    x_seq = np.arange(dimension[1]/5, dimension[1], dimension[1]/5)
    margin_y = 100
    ax.set_yticks(np.arange(0, data_dim/2, margin_y))

    # prediction plot
    plt.subplot(312)
    mat_contents = sio.loadmat(save_path + "Amp_" + str(test_batch_idx + 1) + ".mat")
    pred = mat_contents['amp']
    pred_idx = np.squeeze(pred[idx, :, :data_dim]).transpose()
    ax = sns.heatmap(pred_idx, xticklabels=False)
    ax.set_yticks(np.arange(0, data_dim/2, margin_y))

    # differece plot
    plt.subplot(313)
    diff = label_idx - pred_idx
    ax = sns.heatmap(diff)
    ax.set_xticks(x_seq)
    ax.set_xticklabels(x_seq)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=0)
    ax.set_xlabel('Roundtrips', fontsize=fz)
    ax.set_yticks(np.arange(0, data_dim/2, margin_y))
    plt.savefig(save_path + str(idx+1) + '_amp.png')
    return


def restore_params():
    model = LSTM(in_dim=data_dim,
                 hidden_dim=n_cell,
                 n_class=n_output,
                 bidirectional=lstm_bidirectional,
                 n_layer=lstm_layer,
                 n_dense=dense_layer,
                 n_para=n_para,
                 window=window,
                 dropout=dropout_lstm)
    model.load_state_dict(torch.load(save_path + 'latest.pkl', map_location=device))
    # for param_tensor in model.state_dict():
    #     print(param_tensor, "\t", model.state_dict()[param_tensor].size())
    return model

device = torch.device("cuda:3" if torch.cuda.is_available() else "cpu")
# device = "cpu"
print(device)
# if device == "cuda":
#     # speed the train up
#     torch.backends.cudnn.benchmark = True

train_path_dir = os.listdir(train_path)
n_train_batches = len(train_path_dir)
val_path_dir = os.listdir(val_path)
n_val_batches = len(val_path_dir)
test_path_dir = os.listdir(test_path)
n_test_batches = len(test_path_dir)

if __name__ == '__main__':
    # print(torch.backends.cudnn.version())     # show version of cudnn
    setup_seed(20)
    if train_sign:
        start = time.time()
        # print(Hyper_list)
        HyperFile = open(save_path + "Hyper_para.txt", "w")
        for i in range(len(Hyper_list)):
            HyperFile.write(Hyper_list[i] + "\n")
        HyperFile.close()
        plt.ion()
        train()
        end = time.time()
        plt.ioff()
        plt.show()
        print('Train time: ', end - start, ' sec')
        HyperFile = open(save_path + "Hyper_para.txt", "a")
        HyperFile.write('Train time: ' + str(end - start) + ' sec' + "\n")
        HyperFile.close()
    else:
        # plt.ion()
        # plt.ioff()
        # plt.show()
        if loss_plot:
            # loss plot
            start = time.time()
            test()
            end = time.time()
            print('Inference time: ', end - start, ' sec')
            train_mean_loss = np.loadtxt(save_path + "Training_Mean_Loss.txt")
            test_loss = np.loadtxt(save_path + "Test_Loss.txt")
            test_mean_loss = [np.mean(test_loss) for k in range(len(train_mean_loss))]
            if val_sign:
                val_mean_loss = np.loadtxt(save_path + "Val_Mean_Loss.txt")
                plt.plot(val_mean_loss[0:], 'b-', lw=1, label='val loss')
            plt.plot(train_mean_loss[0:], 'r-', lw=1, label='train loss')
            plt.plot(test_mean_loss[0:], 'y--', lw=1, label='test loss')
            plt.xlabel('Epoch')
            plt.ylabel('MSE')
            plt.legend(loc='upper right')
            plt.title('Loss')
            plt.savefig(save_path + 'loss.png')
        else:
            start = time.time()
            test_v2()
            end = time.time()
            print('Inference time: ', end - start, ' sec')
            HyperFile = open(save_path + "Hyper_para.txt", "a")
            HyperFile.write('Inference time: ' + str(end - start) + ' sec' + "\n")
            HyperFile.close()
            for i in range(20):
                HeatPlot_Amp(0, i)