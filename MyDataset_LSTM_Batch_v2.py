import torch
from torch.utils import data
import os
import numpy as np
import scipy.io as sio
import torch.nn as nn
import mat73

class MyDataset_LSTM_Batch_v2(data.Dataset):
    def __init__(self, root, path, batch_idx, window, data_dim, train=True, test=False):
        path_dir = os.listdir(path)
        if train:
            # mat_contents = sio.loadmat(path + path_dir[batch_idx])
            mat_contents = mat73.loadmat(path + path_dir[batch_idx])
            raw_data = mat_contents['data']
            print("Train %d data loaded:)" % batch_idx)
        elif test:
            mat_contents = mat73.loadmat(path + path_dir[batch_idx])
            raw_data = mat_contents['data']
            print("Test %d data loaded:)" % batch_idx)
        else:   # val set
            mat_contents = mat73.loadmat(path + path_dir[batch_idx])
            raw_data = mat_contents['data']
            print("Val %d data loaded:)" % batch_idx)
        dimension = raw_data.shape      # Num_Samples x Time_Steps x Num_Features
        self.window = window
        self.n_samples = dimension[0]
        self.n_steps = dimension[1]
        self.n_feature = dimension[2]
        self.data_dim = data_dim
        self.raw_data = raw_data
        # Normalization with fixed boundries
        train_path = root + "train/"
        train_path_dir = os.listdir(train_path)
        mat_contents = mat73.loadmat(train_path + "Batch_1.mat")
        temp_data = mat_contents['data']
        self.upper_Bound = np.max(temp_data[:, :, :self.data_dim])     
        self.lower_Bound = np.min(temp_data[:, :, :self.data_dim])
        # Normalization
        raw_data_norm = raw_data
        raw_data_norm[:, :, :self.data_dim] = self.normalization(raw_data[:, :, :self.data_dim])
        # frame segmentation
        self.num_frames = self.n_steps - 1          # num_frames per sample, with padding
        self.num_total_frames = self.num_frames * self.n_samples  # num_frames of all samples
        self.Features = np.zeros((self.num_total_frames, window, self.n_feature))
        self.Labels = np.zeros((self.num_total_frames, self.n_feature))
        self.RT_num = np.zeros((self.num_total_frames, 1))
        for sample in range(self.n_samples):
            # padding (window - 1) first frames
            cur_sample = raw_data_norm[sample, :]
            temp1 = cur_sample[0, :]
            temp2 = np.tile(temp1, (window - 1, 1))
            cur_sample = np.vstack((temp2, cur_sample))
            for frame in range(self.num_frames):
                input = cur_sample[frame:frame+window, :]        # dimension reshape?
                output = cur_sample[frame+window, :]
                frame_pos = sample*self.num_frames + frame
                self.Features[frame_pos, :, :] = input
                self.Labels[frame_pos, :] = output
                self.RT_num[frame_pos] = frame+2                # the roundtrip number for each frame
        # print("Frame segmentation complete:)")
        # print("Feature size: ", self.Features.shape)
        # print("Label size: ", self.Labels.shape)

    def __getitem__(self, index):
        feature = self.Features[index, :, :self.data_dim]     # index: which sample to be chosen
        label = self.Labels[index, :self.data_dim]
        para = self.Labels[index, self.data_dim:]
        # plt.plot(feature[0, :], 'r-', lw=1)       # the 1-st time step
        # plt.plot(label, 'b-', lw=1)               # the label
        # plt.show()
        feature_tensor = torch.from_numpy(feature)
        feature_tensor = feature_tensor.float()
        label_tensor = torch.from_numpy(label)
        label_tensor = label_tensor.float()
        para_tensor = torch.from_numpy(para)
        para_tensor = para_tensor.float()
        rt_num_tensor = torch.tensor(self.RT_num[index])
        rt_num_tensor = rt_num_tensor.float()
        # return feature_tensor, label_tensor, para_tensor, rt_num_tensor
        return feature_tensor, label_tensor, para_tensor

    def __len__(self):
        return self.num_total_frames

    def normalization(self, data):
        _range = self.upper_Bound - self.lower_Bound
        if _range == 0:
            _range = 1e-8
        return (data - self.lower_Bound) / _range

    def anti_normalization(self, data):
        _range = self.upper_Bound - self.lower_Bound
        if _range == 0:
            _range = 1e-8
        data = data * _range + self.lower_Bound
        return data

    def getStartFrames(self):
        feature = np.zeros((self.n_samples, self.window, self.data_dim))
        label = np.zeros((self.n_samples, self.data_dim))
        para = np.zeros((self.n_samples, self.n_feature-self.data_dim))
        # 3. leave the paras alone
        for sample_idx in range(self.n_samples):
            feature[sample_idx, :, :] = self.Features[sample_idx * self.num_frames, :, :self.data_dim]
            label[sample_idx, :] = self.Labels[sample_idx * self.num_frames, :self.data_dim]
            para[sample_idx, :] = self.Labels[sample_idx * self.num_frames, self.data_dim:]
        # plt.plot(feature[0, :], 'r-', lw=1)       # the 1-st time step
        # plt.plot(label, 'b-', lw=1)               # the label
        # plt.show()
        feature_tensor = torch.from_numpy(feature)
        feature_tensor = feature_tensor.float()
        label_tensor = torch.from_numpy(label)
        label_tensor = label_tensor.float()
        para_tensor = torch.from_numpy(para)
        para_tensor = para_tensor.float()
        return feature_tensor, label_tensor, para_tensor


class LSTM(nn.Module):
    def __init__(self, in_dim, hidden_dim, n_class, bidirectional, n_layer=2, n_dense=1, n_para=5, window=10, dropout=0.2):
        super(LSTM, self).__init__()
        self.n_layer = n_layer
        self.hidden_dim = hidden_dim
        self.bidirectional = bidirectional
        self.window = window
        self.lstm = nn.LSTM(in_dim, hidden_dim, n_layer, batch_first=True, dropout=dropout, bidirectional=self.bidirectional)
        self.n_dense = n_dense
        self.n_para = n_para
        if bidirectional:
            self.linear1 = nn.Linear(hidden_dim * 2, n_class)
        else:
            self.linear1 = nn.Linear(hidden_dim, n_class)
            if self.n_dense == 2:
                self.BN1 = nn.BatchNorm1d(n_class)
                self.relu1 = nn.ReLU()
                self.linear2 = nn.Linear(n_class, n_class)
        self.para_layer = nn.Sequential(
                nn.Linear(n_para, n_class),
                nn.LayerNorm(n_class),
        )
        # self.last_RT_layer = nn.Linear(in_dim, hidden_dim)

    def forward(self, x, para=torch.zeros([1, 1])):
        if self.n_para:
            para_x = self.para_layer(para)
            para_x = torch.unsqueeze(para_x, 1)
            para_x = para_x.repeat(1, self.window, 1)
            # sio.savemat("para_x.mat", {'para_x': para_x.cpu().data.numpy()})
            # sio.savemat("x.mat", {'x': x.cpu().data.numpy()})
            x = x + para_x
            # sio.savemat("x_pi.mat", {'x': x.cpu().data.numpy()})
        out, _ = self.lstm(x)
        a, b, c = out.shape
        if self.bidirectional:
            out1 = out[:, -1, :self.hidden_dim]
            out2 = out[:, 0, -self.hidden_dim:]
            output = torch.cat([out1, out2], dim=-1)
            output = self.linear1(output.reshape(-1, c))
        else:
            output = out[:,-1,:]            # output the last state only
            # sio.savemat("output_lstm.mat", {'output': output.cpu().data.numpy()})
            output = self.linear1(output.reshape(-1, c))
            # output = self.linear1(output.reshape(-1, c) + self.last_RT_layer(torch.squeeze(x[:,-1,:])))
            if self.n_dense == 2:
                # output = self.BN1(output)
                output = self.relu1(output)
                output = self.linear2(output)
        return output